﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidSpawner : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject prefab;
    public float radius;
    public int number;
    void Start()
    {
        for(int i =0; i<number; ++i){
            Instantiate(prefab, this.transform.position + Random.insideUnitSphere * radius, Random.rotation);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
